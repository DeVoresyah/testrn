import { useCallback } from "react"
import { atom, useRecoilState, useRecoilValue } from "recoil"
import { apiConfig } from "@stores/config"
import { CartsApi } from "@services/api/carts-api"
import { ProductApi } from "@services/api/product-api"
import { Cart, Product } from "./types"

/**
 * Initial State
 */
const _fetching = atom<boolean>({
  key: "cartFetching",
  default: false,
})
const _data = atom<Cart[] | Cart>({
  key: "cartList",
  default: [],
})
const _error = atom<string>({
  key: "cartError",
  default: "",
})

/**
 * Create custom hooks
 */
export const useCarts = () => {
  const api = useRecoilValue(apiConfig)
  const [fetching, setFetching] = useRecoilState(_fetching)
  const [data, setData] = useRecoilState(_data)
  const [error, setError] = useRecoilState(_error)

  const getCarts = useCallback(async () => {
    setFetching(true)

    const cartApi = new CartsApi(api)
    const productApi = new ProductApi(api)
    const result = await cartApi.getCarts()

    if (result.kind === "ok") {
      const dataToSave = await result.data.map(async (cart) => {
        const getProduct = await Promise.all(cart.products.map(product => productApi.getProduct(product.productId))) as {kind: 'ok', data: Product}[]
        console.tron.log("aaswww =>", getProduct)
        return {
          ...cart,
          products: cart.products.map(subProduct => {
            return {
              ...subProduct,
              ...getProduct.find((subItem) => subItem.data.id === subProduct.productId)
            }
          })
        }
      })
      setData(dataToSave)
      setFetching(false)
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setError(result.kind)
      setFetching(false)
    }
  }, [])

  const getCart = useCallback(async (id: number) => {
    setFetching(true)

    const cartApi = new CartsApi(api)
    const result = await cartApi.getCart(id)

    if (result.kind === "ok") {
      setData(result.data)
      setFetching(false)
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setError(result.kind)
      setFetching(false)
    }
  }, [])

  const getCartsSortDate = useCallback(async (start: string, end:string) => {
    setFetching(true)

    const cartSortDateApi = new CartsApi(api)
    const result = await cartSortDateApi.getCartsSortDate(start, end)

    if (result.kind === "ok") {
      setData(result.data)
      setFetching(false)
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setError(result.kind)
      setFetching(false)
    }
  }, [])

  return {
    fetching,
    data,
    error,
    getCartsSortDate,
    getCarts,
    getCart,
  }
}
