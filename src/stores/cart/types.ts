export interface Product {
  productId: number;
  quality: number;
  id: number;
  title: string;
  price: number;
  category: string;
  description: string;
  image: string;
  rating: {
    rate: number;
    count: number;
  }
}

export interface Cart {
  id: number;
  userId: number;
  date: string;
  products: Product[];
}