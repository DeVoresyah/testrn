import { useEffect } from "react"
import { atom, useRecoilState } from "recoil"
import { load } from "@utils/storage"

// Initial Data
const _status = atom<boolean>({
  key: 'sessionStatus',
  default: false
})

// Custom hooks
export const useSession = () => {
  const [isLoggedIn, setIsLoggedIn] = useRecoilState(_status)

  useEffect(() => {
    ;(async () => {
      const sessionExists = await load("session")
      if (sessionExists) {
        setIsLoggedIn(true)
      } else {
        setIsLoggedIn(false)
      }
    })()
  }, [])

  return {
    isLoggedIn,
    setIsLoggedIn
  }
}