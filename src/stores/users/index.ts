import { useCallback } from "react"
import { atom, useRecoilState, useRecoilValue } from "recoil"
import { apiConfig } from "@stores/config"
import { UserApi } from "@services/api/users-api"
import { User } from "./types"

/**
 * Initial State
 */
const _fetching = atom<boolean>({
  key: "userFetching",
  default: false,
})
const _data = atom<User[]>({
  key: "userList",
  default: []
})
const _error = atom<string>({
  key: "userError",
  default: "",
})
const _detailFetching = atom<boolean>({
  key: "userDetailFetching",
  default: false,
})
const _detail = atom<User | null>({
  key: 'userDetail',
  default: null
})
const _detailError = atom<string>({
  key: "userDetailError",
  default: "",
})

/**
 * Create custom hooks
 */
export const useUsers = () => {
  const api = useRecoilValue(apiConfig)
  const [fetching, setFetching] = useRecoilState(_fetching)
  const [data, setData] = useRecoilState(_data)
  const [error, setError] = useRecoilState(_error)
  const [detailFetching, setDetailFetching] = useRecoilState(_detailFetching)
  const [detail, setDetail] = useRecoilState(_detail)
  const [detailError, setDetailError] = useRecoilState(_detailError)

  const getUsers = useCallback(async () => {
    setFetching(true)

    const usersApi = new UserApi(api)
    const result = await usersApi.GetUsers()

    if (result.kind === "ok") {
      setData(result.data)
      setFetching(false)
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setError(result.kind)
      setFetching(false)
    }
  }, [])

  const getUser = useCallback(async (id: number) => {
    setDetailFetching(true)

    const userApi = new UserApi(api)
    const result = await userApi.GetUser(id)

    if (result.kind === "ok") {
      setDetail(result.data)
      setDetailFetching(false)
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setDetailError(result.kind)
      setDetailFetching(false)
    }
  }, [])

  return {
    fetching,
    data,
    error,
    detail,
    detailFetching,
    detailError,
    getUsers,
    getUser
  }
}
