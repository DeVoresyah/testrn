import { useCallback } from "react"
import { atom, useRecoilState, useRecoilValue } from "recoil"
import { apiConfig } from "@stores/config"
import { AuthApi } from "@services/api/auth-api"
import { Login, LoginPayloadRequest } from "./types"
import { showToast } from "@utils/toast"
import {useSession} from '@stores'
import { save } from "@utils/storage"

/**
 * Initial State
 */
const _fetching = atom<boolean>({
  key: "loginFetching",
  default: false,
})
const _data = atom<Login | null>({
  key: "loginData",
  default: null,
})
const _error = atom<string>({
  key: "loginError",
  default: "",
})

/**
 * Create custom hooks
 */
export const useAuth = () => {
  const api = useRecoilValue(apiConfig)
  const {setIsLoggedIn} = useSession()
  const [fetching, setFetching] = useRecoilState(_fetching)
  const [data, setData] = useRecoilState(_data)
  const [error, setError] = useRecoilState(_error)

  const doLogin = useCallback(async (payload: LoginPayloadRequest) => {
    setFetching(true)

    const authApi = new AuthApi(api)
    const result = await authApi.login(payload)

    if (result.kind === "ok") {
      setData(result.data)
      setFetching(false)
      await save("session", result.data)
      setIsLoggedIn(true)
      showToast("Berhasil masuk!")
    } else {
      __DEV__ && console.tron.log("Error =>", result.kind)
      setError(result.kind)
      setFetching(false)
    }
  }, [])

  return {
    fetching,
    data,
    error,
    doLogin,
  }
}
