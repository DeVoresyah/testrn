export interface Character {
  id: number;
  name: string;
  status: string;
  image: string;
}

export interface LoginPayloadRequest {
  username: string
  password: string
}

export interface Login {
  token: string
}