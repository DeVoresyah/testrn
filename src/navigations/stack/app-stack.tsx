import React from "react"
import { Platform } from "react-native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { createStackNavigator } from "@react-navigation/stack"
import { AppParamList } from "./types"
import { useSession } from "@stores"

// Screens
import { HomeScreen } from "@screens/home"
import { LoginScreen } from "@screens/login"
import { UserDetailScreen } from "@screens/user/detail"
import { CartScreen } from "@screens/cart"

const createStackNav = Platform.OS === "ios" ? createNativeStackNavigator : createStackNavigator

const Stack = createStackNav<AppParamList>()

export const AppStack = () => {
  const {isLoggedIn} = useSession()

  // If not logged in, redirect to login screen
  if (!isLoggedIn) {
    return <LoginScreen />
  }

  return (
    <Stack.Navigator
      initialRouteName="Main"
      screenOptions={{
        headerTitleAlign: "center",
        cardOverlayEnabled: false,
        cardStyleInterpolator: ({ current, layouts }) => ({
          cardStyle: {
            transform: [
              {
                translateX: current.progress.interpolate({
                  inputRange: [0, 1],
                  outputRange: [layouts.screen.width, 0],
                }),
              },
            ],
          },
        }),
      }}
    >
      <Stack.Screen name="Main" component={HomeScreen} options={{ title: 'Dashboard' }} />
      <Stack.Screen name='UserDetail' component={UserDetailScreen} options={{ title: 'User Detail' }} />
      <Stack.Screen name='Carts' component={CartScreen} options={{ title: 'Carts' }} />
    </Stack.Navigator>
  )
}
