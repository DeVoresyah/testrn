import React, {FC, memo} from 'react'
import {FlatList} from 'react-native'
import {User} from '@stores/users/types'

// Components
import { UserCard } from '@components/molecule'

// Styles
import { apply } from '@theme'

// Component Props
export interface UserListProps {
  data: User[]
}

export const UserList: FC<UserListProps> = memo((props) => {
  const {data} = props

  return (
    <FlatList 
      data={data}
      extraData={data}
      keyExtractor={(_, index) => index.toString()}
      initialNumToRender={6}
      renderItem={({ item, index }) => <UserCard data={item} cardStyle={index !== data.length - 1 && apply('mb-3')} />}
      showsVerticalScrollIndicator={false}
    />
  )
})