import React, {FC, memo} from 'react'
import {ActivityIndicator, View} from 'react-native'

// Styles
import { apply } from '@theme'

// Component Props
export interface LoadingLayoutProps {}

export const LoadingLayout: FC<LoadingLayoutProps> = memo(() => {
  return (
    <View style={apply('flex bg-gray-100 items-center justify-center')}>
      <ActivityIndicator size='large' color={apply('blue-500')} />
    </View>
  )
})