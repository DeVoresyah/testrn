import React, {FC, memo} from 'react'
import {StyleProp, ViewStyle, TextInputProps, TextInput, View, Text} from 'react-native'

// Styles
import { apply } from '@theme'
import styles from './styles'

// Component Props
export interface InputProps extends TextInputProps {
  containerStyle?: StyleProp<ViewStyle>
  label: string | number
  error?: string
}

export const Input: FC<InputProps> = memo(props => {
  const { containerStyle, label, error} = props

  return (
    <View style={[apply('full'), containerStyle]}>
      <Text style={styles.label}>{label}</Text>
      <TextInput 
        {...props}
        placeholderTextColor={apply('gray-400')}
        style={styles.inputContainer}
      />
      {error !== '' && <Text style={styles.error}>{error}</Text>}
    </View>
  )
})