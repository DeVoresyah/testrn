import { connect } from "@theme";

export default connect({
  label: 'text-sm font-bold text-gray-900 mb-1',
  inputContainer: 'rounded-lg bg-gray-100 py-2 px-4 border border-gray-400 text-gray-900',
  error: 'text-red-500 text-xs font-medium mt-1'
})