import React, {FC, memo, useMemo} from 'react'
import {StyleProp, ViewStyle, TouchableOpacityProps, TouchableOpacity, ActivityIndicator, Text} from 'react-native'

// Styles
import { apply } from '@theme'
import styles from './styles'

// Component Props
export interface ButtonProps extends TouchableOpacityProps {
  buttonStyle?: StyleProp<ViewStyle>
  loading?: boolean
  title?: string | number
}

export const Button: FC<ButtonProps> = memo((props) => {
  const {buttonStyle, loading, title, disabled} = props

  // Button Content
  const buttonContent = useMemo(() => {
    if (loading) {
      return <ActivityIndicator size='small' color='white' />
    } else {
      return <Text style={styles.title}>{title}</Text>
    }
  }, [loading, title])

  // Button Background
  const buttonBg = useMemo(() => {
    if (loading || disabled) {
      return apply('bg-blue-500 bg-opacity-50')
    }

    return null
  }, [loading, disabled])

  return (
    <TouchableOpacity {...props} activeOpacity={0.8} style={[buttonStyle, styles.container, buttonBg]}>
      {buttonContent}
    </TouchableOpacity>
  )
})