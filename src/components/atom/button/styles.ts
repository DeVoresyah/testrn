import { connect } from "@theme";

export default connect({
  container: 'bg-blue-500 rounded-lg py-2 px-4 items-center',
  title: 'text-base font-medium text-white text-center'
})