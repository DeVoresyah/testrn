import React, {FC, memo} from 'react'
import {StyleProp, ViewStyle, TouchableOpacity} from 'react-native'

// Styles
import styles from './styles'

export interface CardProps {
  cardStyle?: StyleProp<ViewStyle>
  onPress?: () => void
}

export const Card: FC<CardProps> = memo((props) => {
  const {cardStyle, onPress} = props

  return (
    <TouchableOpacity activeOpacity={onPress ? 0.9 : 1} style={[cardStyle, styles.container]} onPress={() => onPress?.()}>
      {props.children}
    </TouchableOpacity>
  )
})