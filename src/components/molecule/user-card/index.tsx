import React, {FC, useCallback, useMemo, memo} from 'react'
import {StyleProp, ViewStyle, Text} from 'react-native'
import {User} from '@stores/users/types'
import { useNavigation } from '@react-navigation/native'
import { RootStackNavProps } from '@navigations/stack/types'
import { useUsers } from '@stores'

// Component
import { Card } from '@components/atom'

// Styles
import styles from './styles'

// Component Props
export interface UserCardProps {
  cardStyle?: StyleProp<ViewStyle>
  data: User
}

export const UserCard: FC<UserCardProps> = memo((props) => {
  const {cardStyle, data} = props
  const {getUser} = useUsers()
  const navigation = useNavigation<RootStackNavProps<'Main'>>()

  // Card onPress handler
  const onDetail = useCallback(async () => {
    await getUser(data.id)
    navigation.navigate("UserDetail")
  }, [data, getUser, navigation])

  // Full name
  const fullName = useMemo(() => `${data.name.firstname} ${data.name.lastname}`, [data])

  return (
    <Card cardStyle={cardStyle} onPress={onDetail}>
      <Text style={styles.title}>{fullName}</Text>
      <Text style={styles.desc}>{data?.email}</Text>
    </Card>
  )
})