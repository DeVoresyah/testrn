import { connect } from "@theme";

export default connect({
  title: 'font-bold text-base text-gray-900',
  desc: 'font-medium text-sm text-gray-400'
})