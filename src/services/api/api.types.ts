import { GeneralApiProblem } from './api-problem';
import { Character, Login } from '@stores/auth/types';
import { User } from '@stores/users/types';
import { Cart, Product } from '@stores/cart/types';

export type GetCharactersResult =
  | { kind: 'ok'; data: Character[] }
  | GeneralApiProblem;
  
export type GetCartsResult=
  | { kind: 'ok'; data: Cart[] }
  | GeneralApiProblem;

export type DetailCartResult=
  | { kind: 'ok'; data: Cart }
  | GeneralApiProblem;

export type LoginResult =
  | { kind: 'ok'; data: Login }
  | GeneralApiProblem;

export type UsersResult =
  | { kind: 'ok'; data: User[] }
  | GeneralApiProblem;

export type DetailUserResult =
  | { kind: 'ok'; data: User }
  | GeneralApiProblem;

export type DetailProductResult = { kind: 'ok', data: Product } | GeneralApiProblem