import { ApiResponse } from "apisauce"
import { Api } from "./api"
import { GetCartsResult, DetailCartResult } from "./api.types"
import { getGeneralApiProblem } from "./api-problem"

export class CartsApi {
  private api: Api

  constructor(api: Api) {
    this.api = api
  }

  async getCarts(): Promise<GetCartsResult> {
    try {
      const vm = this
      const response: ApiResponse<any> = await this.api.apisauce.get(
        "/carts",
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }
      
      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }

  async getCart(id: number): Promise<DetailCartResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.post(
        `/cart${id}`,
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }

  async getCartsSortDate(start:string, end:string): Promise<GetCartsResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.post(
        `/carts/startdate=${start}&enddate=${end}`,
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }
}