import { ApiResponse } from "apisauce"
import { Api } from "./api"
import { DetailProductResult } from "./api.types"
import { getGeneralApiProblem } from "./api-problem"

export class ProductApi {
  private api: Api

  constructor(api: Api) {
    this.api = api
  }

  async getProduct(id: number): Promise<DetailProductResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.get(
        `/products/${id}`,
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch(e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }
}