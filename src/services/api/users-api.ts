import { ApiResponse } from "apisauce"
import { Api } from "./api"
import { UsersResult, DetailUserResult } from "./api.types"
import { getGeneralApiProblem } from "./api-problem"

export class UserApi {
  private api: Api

  constructor(api: Api) {
    this.api = api
  }

  async GetUsers(): Promise<UsersResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.get(
        "/users",
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }

  async GetUser(id: number): Promise<DetailUserResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.get(
        `/users/${id}`,
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      console.log("response erro =>", e)
      return { kind: "bad-data" }
    }
  }
}