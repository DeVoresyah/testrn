import { ApiResponse } from "apisauce"
import { Api } from "./api"
import { LoginResult } from "./api.types"
import { getGeneralApiProblem } from "./api-problem"
import {LoginPayloadRequest} from '@stores/auth/types'

export class AuthApi {
  private api: Api

  constructor(api: Api) {
    this.api = api
  }

  async login(payload: LoginPayloadRequest): Promise<LoginResult> {
    try {
      const response: ApiResponse<any> = await this.api.apisauce.post(
        "/auth/login",
        JSON.stringify(payload)
      )

      if (!response.ok) {
        const problem = getGeneralApiProblem(response)
        if (problem) return problem
      }

      return { kind: "ok", data: response.data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      return { kind: "bad-data" }
    }
  }
}