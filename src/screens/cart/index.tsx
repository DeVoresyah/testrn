import React, { FC, useEffect } from "react"
import { Text } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"
import { RootStackScreenProps } from "@navigations/stack/types"
import { useCarts } from "@stores"

// Components
import { Card } from "@components/atom"
import { LoadingLayout } from "@components/organism"

// Styles
import styles from './styles'

export const CartScreen: FC<RootStackScreenProps<'Carts'>> = props => {
  const {fetching, data, getCarts } = useCarts()

  useEffect(() => {
    ;(async () => {
      await getCarts()
    })()
  }, [])

  console.tron.log("carts =>", data)

  return fetching ? <LoadingLayout /> : (
    <SafeAreaView edges={['left', 'right']} style={styles.container}>

    </SafeAreaView>
  )
}