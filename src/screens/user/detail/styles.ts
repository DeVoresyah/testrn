import { connect } from "@theme";

export default connect({
  container: 'flex p-5 bg-gray-100',
  card: 'mb-3 flex-wrap',
  info: 'font-medium text-xs text-gray-400 mb-1',
  title: 'font-medium text-sm text-gray-900',
})