import React, { FC, useMemo } from "react"
import { Text } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"
import { RootStackScreenProps } from "@navigations/stack/types"
import { useUsers } from "@stores"

// Components
import { Card } from "@components/atom"
import { LoadingLayout } from "@components/organism"

// Styles
import styles from './styles'

export const UserDetailScreen: FC<RootStackScreenProps<'UserDetail'>> = props => {
  const {detailFetching, detail} = useUsers()
  
  // Get full name
  const fullName = useMemo(() => `${detail?.name?.firstname} ${detail?.name?.lastname}`, [detail])

  // Get full address
  const fullAddress = useMemo(() => `${detail?.address?.street} ${detail?.address?.number}, ${detail?.address?.city}, ${detail?.address?.zipcode}`, [detail])

  return detailFetching ? <LoadingLayout /> : (
    <SafeAreaView edges={['left', 'right']} style={styles.container}>
      <Card cardStyle={styles.card}>
        <Text style={styles.info}>Name</Text>
        <Text style={styles.title}>{fullName}</Text>
      </Card>

      <Card cardStyle={styles.card}>
        <Text style={styles.info}>Email</Text>
        <Text style={styles.title}>{detail?.email}</Text>
      </Card>

      <Card cardStyle={styles.card}>
        <Text style={styles.info}>Phone</Text>
        <Text style={styles.title}>{detail?.phone}</Text>
      </Card>

      <Card cardStyle={styles.card}>
        <Text style={styles.info}>Address</Text>
        <Text style={styles.title}>{fullAddress}</Text>
      </Card>
    </SafeAreaView>
  )
}