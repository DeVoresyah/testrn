import React, { FC, useEffect } from "react"
import { ActivityIndicator, TouchableOpacity } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"
import { RootStackScreenProps } from "@navigations/stack/types"
import {useUsers} from '@stores'

// Icons
import Icons from 'react-native-vector-icons/Feather'

// Components
import { UserList } from "@components/organism"

// Styles
import styles from "./style"
import { apply } from "@theme"

export const HomeScreen: FC<RootStackScreenProps<"Main">> = ({ navigation }) => {
  const users = useUsers()

  // Navigation config
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity activeOpacity={1} style={apply('mr-5')} onPress={() => navigation.navigate('Carts')}>
          <Icons name='shopping-cart' size={24} color={apply('gray-900')} />
        </TouchableOpacity>
      ),
    })
  }, [navigation])

  useEffect(() => {
    ;(async () => {
      await users.getUsers()
    })()
  }, [])

  return (
    <SafeAreaView edges={['left', 'right']} style={styles.container}>
      {users.fetching ? <ActivityIndicator size='large' color={apply('blue-500')} /> : <UserList data={users.data} />}
    </SafeAreaView>
  )
}
