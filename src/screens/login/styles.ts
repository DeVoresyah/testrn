import {connect} from '@theme'

export default connect({
  title: 'text-2xl font-bold text-gray-900 text-center mb-12',
  container: 'flex bg-white p-5 items-center justify-center',
  formWrapper: 'mb-3',
  button: 'full mt-5'
})