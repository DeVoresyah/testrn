import React from 'react'
import {Text} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Controller, useForm } from 'react-hook-form'
import {useAuth} from '@stores'

// Components
import { Button, Input } from '@components/atom'

// Styles
import styles from './styles'

// Type definition
type FormLogin= {
  username: string,
  password: string
}

export const LoginScreen: React.FC = () => {
  const auth = useAuth()
  const {control, handleSubmit, formState: { isDirty, isValid, errors } } = useForm<FormLogin>({
    defaultValues: {
      username: '',
      password: ''
    },
    mode: 'onChange',
    reValidateMode: 'onChange'
  })

  // Form submit handler
  const onSubmit = handleSubmit(({ username, password }) => {
    const dataToSend = {
      username,
      password
    }

    auth.doLogin(dataToSend)
  })

  return (
    <SafeAreaView edges={['left', 'right']} style={styles.container}>
      <Text style={styles.title}>Hello App</Text>

      <Controller
        name='username'
        control={control}
        rules={{ required: { value: true, message: 'Username tidak boleh kosong.' } }}
        render={({ field: { onChange, value } }) => (
          <Input 
            label='Username'
            value={value}
            onChangeText={onChange}
            error={errors?.username?.message ?? ''}
            containerStyle={styles.formWrapper}
            placeholder='Masukkan username'
          />
        )}
      />

      <Controller
        name='password'
        control={control}
        rules={{ required: { value: true, message: 'Password tidak boleh kosong.' } }}
        render={({ field: { onChange, value } }) => (
          <Input 
            label='Password'
            value={value}
            onChangeText={onChange}
            error={errors?.password?.message ?? ''}
            containerStyle={styles.formWrapper}
            placeholder='Masukkan password'
            secureTextEntry={true}
          />
        )}
      />

      <Button title="Masuk" buttonStyle={styles.button} loading={auth.fetching} disabled={!isDirty || !isValid} onPress={onSubmit} />
    </SafeAreaView>
  )
}